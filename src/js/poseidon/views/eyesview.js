(function(namespace){

  var $eyeLids = $('#eye-lids');

  namespace.EyesView = new (Backbone.View.extend({
    el: $eyeLids,
    openTop: 650,

    initialize: function(){
      _.bindAll(this);
      this.closedTop = parseInt(this.$el.css('top'));
    },

    open: function(){
      var eyesView = this;
      this.$el.animate({
        top: eyesView.openTop
      }, 3000);
    },

    close: function() {
      var eyesView = this;
      this.$el.animate({
        top: eyesView.closedTop
      }, 3000);
    }

  }));
})(window.poseidon);