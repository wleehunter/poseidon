(function(namespace){

  var $content = $('#content-wrapper');

  namespace.LoaderView = new (Backbone.View.extend({
    el: $content,
    images: {
      "../images/base.png": false,
      "../images/lightning.png": false,
      "../images/bicep.png": false,
      "../images/forearm_trident.png": false,
      "../images/overlay.png": false
    },
    allLoaded: false,

    initialize: function(){
      _.bindAll(this);
      var _this = this;
      this.loadImages();
    },

    loadImages: function() {
      var _this = this;
      $.each(this.images,function(image,v){
        var img = new Image();
        img.onload = function() {
          _this.images[image] = true;
          _this.checkImages();
        }
        img.src = image;
      });
    },

    checkImages: function() {
      var allLoaded = true;
      $.each(this.images,function(k,loaded){
        if(!loaded) {
          allLoaded = false;
        }
      });
      if(allLoaded) {
        this.allLoaded = true;
        // $content.fadeIn(1000);
      }
    }

  }));
})(window.poseidon);