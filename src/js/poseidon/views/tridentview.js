(function(namespace){

  var Trident = namespace.Trident;

  var $content = $('#content-wrapper');
  var $bicep = $('#bicep');
  var $trident = $('#forearm-trident');

  namespace.TridentView = new (Backbone.View.extend({
    el: $bicep,

    bicepAngleOffset: 101,
    bicepRadius: 180,
    bicepOffset: {
      "x": 76,
      "y": 213
    },
    
    tridentOffset: {
      "x": 317,
      "y": 545
    },

    initialize: function(){
      _.bindAll(this);
      var tridentView = this;
      this.model = Trident;
    },

    slamTrident: function(context) {
      var tridentView = this;

      var shoulderCircle = {
        "x": parseInt(tridentView.$el.css('left')) + tridentView.bicepOffset.x,
        "y": parseInt(tridentView.$el.css('top')) + tridentView.bicepOffset.y,
        "radius": tridentView.bicepRadius
      }
      var reachedTop = false;
      var reset = false;

      var tridentInterval = window.setInterval(function(){
        var angle = tridentView.getTridentAngle();
        if(!reachedTop) {
          angle++;
        }
        else {
          angle -= 20;
        }
        
        tridentView.$el.css({
          '-webkit-transform': 'rotate(' + angle + 'deg)',
          '-moz-transform': 'rotate(' + angle + 'deg)',
          '-ms-transform': 'rotate(' + angle + 'deg)',
          '-o-transform': 'rotate(' + angle + 'deg)',
          'transform': 'rotate(' + angle + 'deg)'
        });

        var bicepAngle = angle + tridentView.bicepAngleOffset;
        var bicepRadians = bicepAngle * (Math.PI / 180);

        var elbowPoint = tridentView.getElbowPoint(bicepRadians, shoulderCircle);
        $trident.css({
          "left": elbowPoint.x,
          "top": elbowPoint.y
        });
        if(angle >= 120) {
          reachedTop = true;
        }
        if(reachedTop && angle < 40) {
          window.clearInterval(tridentInterval);
          tridentView.model.trigger('slammed');
        }
      },20)
    },

    getElbowPoint: function(radians, circle) {
      var x = (circle.x + circle.radius * Math.cos(radians)) - this.tridentOffset.x;
      var y = (circle.y + circle.radius * Math.sin(radians)) - this.tridentOffset.y;
      var ret = {
              "x": x,
              "y": y
            };
      return ret;
    },

    getTridentAngle: function(){
      var tridentView = this;
      var st = window.getComputedStyle(tridentView.$el[0], null);
      var tr = st.getPropertyValue("-webkit-transform") ||
               st.getPropertyValue("-moz-transform") ||
               st.getPropertyValue("-ms-transform") ||
               st.getPropertyValue("-o-transform") ||
               st.getPropertyValue("transform") ||
               null;

      // With rotate(30deg)...
      // matrix(0.866025, 0.5, -0.5, 0.866025, 0px, 0px)

      // If no matrix is set just set the angle to 
      if(tr == 'none'){ 
        tr = "matrix(1, 0, 0, 1, 0, 0)";
      }
      //console.log('Matrix: ' + tr);
      // rotation matrix - http://en.wikipedia.org/wiki/Rotation_matrix

      var values = tr.split('(')[1];
          values = values.split(')')[0];
          values = values.split(',');
      var a = values[0];
      var b = values[1];
      var c = values[2];
      var d = values[3];

      var scale = Math.sqrt(a*a + b*b);
      var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
      
      return angle;
    }

  }));
})(window.poseidon);