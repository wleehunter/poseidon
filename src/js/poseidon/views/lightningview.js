(function(namespace){

  var $lightning = $('#lightning');
  var Trident = namespace.Trident;

  namespace.LightningView = new (Backbone.View.extend({
    el: $lightning,

    initialize: function(){
      _.bindAll(this);
      var lightningView = this;
      Trident.on('slammed', function(){
        lightningView.render();
      })
    },

    render: function(){
      var lightningView = this;

      var i = 3;
      var first = 1;
      var second = 2;
      var lightningInterval = window.setInterval(function(){
        if(i == first + second) {
          first = second;
          second = i;
          lightningView.$el.show();
        }
        else {
          lightningView.$el.hide();
        }

        i++;
        if(i > 34) {  
          window.clearInterval(lightningInterval);
          lightningView.$el.fadeOut();
        }
      }, 75);
    }

  }));
})(window.poseidon);