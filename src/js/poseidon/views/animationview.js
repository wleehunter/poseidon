(function(namespace){

  var Audio = namespace.Audio;
  var Trident = namespace.TridentView;
  var Eyes = namespace.EyesView;

  var $content = $('#content-wrapper');

  namespace.AnimationView = new (Backbone.View.extend({
    el: $content,

    started: false,

    initialize: function(){
      _.bindAll(this);
      var animationView = this;

      Audio.on('play', function(){
        if(!animationView.started) {
          animationView.start();
          animationView.started = true;
        }
      });
      Audio.on('ended', function(){
        if(animationView.started) {
          window.setTimeout(function(){
            Eyes.close();
          }, 3000);
          animationView.started = false;
        }
      });
    },

    start: function() {
      var animationView = this;
      this.$el.fadeIn(1500);

      window.setTimeout(function(){
        Eyes.open();
      }, 11000);

      window.setTimeout(function(){
        if(animationView.started) {
          Trident.slamTrident();
        }
      }, 7850);
      window.setTimeout(function(){
        if(animationView.started) {
          Trident.slamTrident();
        }
      }, 14450);
      window.setTimeout(function(){
        if(animationView.started) {
          Trident.slamTrident();
        }
      }, 20750);
      window.setTimeout(function(){
        if(animationView.started) {
          Trident.slamTrident();
          Trident.model.on('slammed',function() {
            if(animationView.started) {
              animationView.startRandomTrident();
            }
          })
        }
      }, 23850);
    },

    startRandomTrident: function() {
      var animationView = this;
      var random = parseInt((Math.random() * 6000));
      console.log("random " + random);
      window.setTimeout(function(){
        if(animationView.started) {
          Trident.slamTrident();
        }
      }, random);
    },





  }));
})(window.poseidon);