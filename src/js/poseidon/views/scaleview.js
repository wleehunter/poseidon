(function(namespace){

  var $content = $('#content-wrapper');
  var $audio = $('#audio-controls');

  namespace.ScaleView = new (Backbone.View.extend({
    el: $content,

    initialize: function(){
      _.bindAll(this);
      var _this = this;
      $(window).resize(function(){
        _this.scaleContent();
      });
      this.scaleContent();
    },

    scaleContent: function() {
      var windowWidth = parseInt($(window).width());
      var windowHeight = parseInt($(window).height()) - parseInt($audio.height());
      var contentWidth = parseInt($content.width());
      var contentHeight = parseInt($content.height());
      var imageRatio = contentHeight / contentWidth;
      var viewportRatio = windowHeight / windowWidth;

      // portrait
      if(windowWidth < windowHeight) {
        var scale = (windowWidth / contentWidth) * .9;
        var center = (windowWidth - (contentWidth * scale));
        $content.transform({
          scaleX: scale,
          scaleY: scale,
          origin: [0 , 0]
          // origin: [center + 'px' , 0]
        })
      }
      // landscape
      else {
        var scale = (imageRatio < viewportRatio) ?
          windowWidth / contentWidth : windowHeight / contentHeight;
        // var scale = windowHeight / contentHeight;
        var center = parseInt(contentWidth / 2);
        var center = (windowWidth - (contentWidth * scale)) / 2;
        $content.transform({
          scaleX: scale,
          scaleY: scale,
          origin: [0 , 0]
        });
      }

      var leftMargin = (windowWidth - (contentWidth * scale)) / 2;

      $content.css({
        marginLeft: leftMargin
      })
      
    }

  }));
})(window.poseidon);