(function(namespace){

  var Audio = namespace.Audio;

  var $loading = $('#loading');
  var $ellipsis = $('#ellipsis');
  var $audio = $('#audio-controls');
  var $play = $('#play');
  var $pause = $('#pause');
  var $stop = $('#stop');

  namespace.AudioView = new (Backbone.View.extend({
    el: $audio,
    played: false,
    context: null,
    audioBuffer: null,
    source: null,
    currentTime: 0,
    loaderInterval: null,

    initialize: function(){
      _.bindAll(this);
      var audioView = this;

      this.model = Audio;

      this.setLoaderInterval();

      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      this.context = new AudioContext();

      this.loadAudio();

      $play.click(function(){
        audioView.play(audioView.currentTime);
      });

      $pause.click(function(){
        audioView.pause();
      });
      $stop.click(function(){
        audioView.stop();
      });
    },

    setLoaderInterval: function() {
      var i = 1;
      var text = "";
      this.loaderInterval = setInterval(function(){
        if(i % 4 == 0) {
          text = "";
        }
        else {
          text += "."
        }
        $ellipsis.text(text);
        i++;
      },400);
    },

    loadAudio: function() {
      var audioView = this;
      var request = new XMLHttpRequest();
      request.open('GET', '/audio/poseidon_audio.mp3', true);
      request.responseType = 'arraybuffer';

      // Decode asynchronously
      request.onload = function() {
        audioView.context.decodeAudioData(request.response, function(buffer) {
          audioView.audioBuffer = buffer;
          $loading.hide();
          clearInterval(audioView.loaderInterval);
          $audio.fadeIn(1000);
        }, onError);
      };
      onError = function(error) {
        console.log(error);
      };
      request.send();
    },

    play: function(startTime) {
      var audioView = this;
      this.source = this.context.createBufferSource(); // creates a sound source
      this.source.buffer = this.audioBuffer;                    // tell the source which sound to play
      this.source.connect(this.context.destination);       // connect the source to the context's destination (the speakers)

      this.source.onended = function(){
        audioView.onEnded();
      };

      this.source.start(startTime);
      this.model.trigger('play');
      //this.startTimer();
      this.$el.removeClass('intro');
    },

    onEnded: function() {
      this.model.trigger('ended');
    },

    pause: function() {
      this.source.stop();
    },

    stop: function() {
      this.source.stop(0);
      this.currentTime = 0;
    },

    startTimer: function() {
      var date = new Date();
      var startTime = date.getTime();
      var i = 0;
      this.timerInterval = setInterval(function(){
        $('#timer').text(i);
        i += .05;
      },50);
      $(document).on('click', function(){
        if(i > 0) {
          var newDate = new Date();
          var newTime = newDate.getTime();
          var intervalSeconds = ((newTime - startTime) / 1000) + " and i= " + i;
          var interval = $('<li>').text(intervalSeconds).appendTo($('#time-marks'));
        }
      })
    }

  }));
})(window.poseidon);